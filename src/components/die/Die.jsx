import React, { Fragment } from "react";

const Die = () => {
  return (
    <Fragment>
      Die Leitner Box ist eine Lernhilfe oder ein Karteikartensystem, das
      Lernenden hilft, sich Informationen effizient zu merken. Es wurde in den
      1970er Jahren vom deutschen Psychologen Sebastian Leitner entwickelt.{" "}
      <br /> <br />
      Die Leitner-Box besteht aus einem Satz Karteikarten oder Karteikarten, auf
      deren einer Seite jeweils eine Frage oder Information und auf der anderen
      Seite die Antwort steht. Die Karten sind in mehrere Kästchen,
      typischerweise vier oder fünf, unterteilt, die mit zunehmendem
      Schwierigkeitsgrad beschriftet sind. <br /> <br />
      Um das Leitner-Box-System zu verwenden, überprüfen die Lernenden zunächst
      die Karten in der ersten Box. Wenn sie eine Frage richtig beantworten,
      wird die Karte in das nächste Feld verschoben. Bei einer falschen Antwort
      wird die Karte in das erste Feld zurückgelegt.
    </Fragment>
  );
};

export default Die;
