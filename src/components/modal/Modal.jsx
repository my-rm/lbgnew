import React, { Fragment } from "react";
import classess from "./Modal.module.css";
import { Box, IconButton } from "@mui/material";
import ArrowBack from "@mui/icons-material/ArrowBackIosRounded";

const Modal = ({ W, ModalConfig, setModalConfig }) => {
  return (
    <Fragment>
      <Box
        className={classess.Modal}
        sx={{
          animation:
            W <= 560
              ? ModalConfig === true
                ? "RTL 0.3s ease-in-out 0s 1 forwards"
                : ModalConfig === false
                ? "LTR 0.3s ease-in-out 0s 1 backwards"
                : null
              : null,
        }}
      >
        <Box className={classess.Kasten}>
          <IconButton
            sx={{
              width: "40px",
              height: "40px",
              marginLeft: "-14px",
              marginTop: "-7px",
            }}
            onClick={() => setModalConfig(false)}
          >
            <ArrowBack sx={{ cursor: "pointer", color: "#707070" }} />
          </IconButton>
        </Box>
      </Box>
    </Fragment>
  );
};

export default Modal;
