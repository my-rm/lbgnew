import "./App.css";
import React, { Fragment, useEffect, useState } from "react";
import UiDimensions from "./context/UiDimensions";
import Kaffee from "./pages/kaffee/Kaffee";
import Sozial from "./pages/sozial/Sozial";
import Dieinfo from "./pages/dieinfo/Dieinfo";
import Modal from "./components/modal/Modal";

const App = () => {
  const Width = UiDimensions().innerWidth;
  const Height = UiDimensions().innerHeight;
  const [ModalConfig, setModalConfig] = useState(null);
  useEffect(() => {
    if (Width <= 560) setModalConfig(null);
  }, [Width]);

  return (
    <Fragment>
      <Modal
        W={Width}
        H={Height}
        ModalConfig={ModalConfig}
        setModalConfig={setModalConfig}
      />
      <Kaffee W={Width} H={Height} />
      <Sozial W={Width} H={Height} />
      <Dieinfo W={Width} H={Height} setModalConfig={setModalConfig} />
    </Fragment>
  );
};

export default App;
