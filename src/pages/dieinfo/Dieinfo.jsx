import React from "react";
import { Fragment } from "react";
import classess from "./Dieinfo.module.css";
import { Box } from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import Die from "../../components/die/Die";

const Dieinfo = ({ setModalConfig }) => {
  return (
    <Fragment>
      <Box className={classess.Dieinfo}>
        <Box className={classess.Gelb}></Box>
        <Box className={classess.Info}>
          <Box className={classess.Rot}></Box>
          <span className={classess.DU}>DU WEIßT Über</span>
          <span className={classess.LB}>Lietner Box</span>
          <Box className={classess.DIE}>
            <Die />
          </Box>
          <Box className={classess.Zeig}>
            <span className="AUL" onClick={() => setModalConfig(true)}>
              ZEIG MEHR{" "}
              <ArrowForwardIcon
                sx={{
                  width: "12px",
                  height: "auto",
                  position: "relative",
                  top: "2px",
                }}
              />
            </span>
          </Box>
        </Box>
      </Box>
    </Fragment>
  );
};

export default Dieinfo;
