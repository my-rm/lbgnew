import { Box } from "@mui/material";
import React, { Fragment } from "react";
import classess from "./Kaffee.module.css";
import COFFEE1 from "../../assets/COFFEE1.png";

const Kaffee = () => {
  return (
    <Fragment>
      <Box className={classess.Kaffee}>
        <Box className={classess.Gelb}>
          <img src={COFFEE1} alt="" />
          <Box className={classess.Portfolio}>
            <span>Portfolio</span>
            <span>Leitner Box</span>
          </Box>
        </Box>
      </Box>
    </Fragment>
  );
};

export default Kaffee;
