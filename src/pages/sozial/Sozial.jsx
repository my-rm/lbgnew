import { Box } from "@mui/material";
import React, { Fragment, useState } from "react";
import classess from "./Sozial.module.css";
import { SocialIcon } from "react-social-icons";

const Sozial = (prospe) => {
  const [EL, setEL] = useState("");
  return (
    <Fragment>
      <Box className={classess.Sozial}>
        <span className={classess.Copyright}>
          Copyright © Dezember-2021. Alle Rechte vorbehalten. Erstellt von Ramin
          Maghsoodi
        </span>
        <Box>
          <SocialIcon
            url="instagram"
            bgColor={
              EL === "instagram_RED"
                ? "rgba(90,90,90,0.1)"
                : "rgba(100,100,100,0.0)"
            }
            fgColor={EL === "instagram_RED" ? "#ed013e" : "#444444"}
            style={{
              width: prospe.W <= 560 ? "30px" : "33px",
              height: prospe.W <= 560 ? "30px" : "33px",
              marginLeft: prospe.W <= 560 ? "30px" : "40px",
            }}
            href="#"
            onMouseEnter={() => setEL("instagram_RED")}
            onMouseLeave={() => setEL("instagram")}
          />
          <SocialIcon
            url="dribbble"
            bgColor="rgba(100,100,100,0.0)"
            fgColor="#ed013e"
            style={{
              width: prospe.W <= 560 ? "30px" : "33px",
              height: prospe.W <= 560 ? "30px" : "33px",
              marginLeft: "-7px",
            }}
            href="#"
          />
          <SocialIcon
            url="linkedin"
            bgColor={
              EL === "linkedin_RED"
                ? "rgba(90,90,90,0.1)"
                : "rgba(100,100,100,0.0)"
            }
            fgColor={EL === "linkedin_RED" ? "#ed013e" : "#444444"}
            style={{
              width: prospe.W <= 560 ? "30px" : "33px",
              height: prospe.W <= 560 ? "30px" : "33px",
              marginLeft: "-7px",
            }}
            href="#"
            onMouseEnter={() => setEL("linkedin_RED")}
            onMouseLeave={() => setEL("linkedin")}
          />
          <SocialIcon
            url="youtube"
            bgColor={
              EL === "youtube_RED"
                ? "rgba(90,90,90,0.1)"
                : "rgba(100,100,100,0.0)"
            }
            fgColor={EL === "youtube_RED" ? "#ed013e" : "#444444"}
            style={{
              width: prospe.W <= 560 ? "30px" : "33px",
              height: prospe.W <= 560 ? "30px" : "33px",
              marginLeft: "-7px",
            }}
            href="#"
            onMouseEnter={() => setEL("youtube_RED")}
            onMouseLeave={() => setEL("youtube")}
          />
          <SocialIcon
            url="pinterest"
            bgColor={
              EL === "pinterest_RED"
                ? "rgba(90,90,90,0.1)"
                : "rgba(100,100,100,0.0)"
            }
            fgColor={EL === "pinterest_RED" ? "#ed013e" : "#444444"}
            style={{
              width: prospe.W <= 560 ? "30px" : "33px",
              height: prospe.W <= 560 ? "30px" : "33px",
              marginLeft: "-7px",
            }}
            href="#"
            onMouseEnter={() => setEL("pinterest_RED")}
            onMouseLeave={() => setEL("pinterest")}
          />
          <SocialIcon
            url="twitter"
            bgColor={
              EL === "twitter_RED"
                ? "rgba(90,90,90,0.1)"
                : "rgba(100,100,100,0.0)"
            }
            fgColor={EL === "twitter_RED" ? "#ed013e" : "#444444"}
            style={{
              width: prospe.W <= 560 ? "30px" : "33px",
              height: prospe.W <= 560 ? "30px" : "33px",
              marginLeft: "-7px",
            }}
            href="#"
            onMouseEnter={() => setEL("twitter_RED")}
            onMouseLeave={() => setEL("twitter")}
          />
          <SocialIcon
            url="gitlab"
            bgColor={
              EL === "gitlab_RED"
                ? "rgba(90,90,90,0.1)"
                : "rgba(100,100,100,0.0)"
            }
            fgColor={EL === "gitlab_RED" ? "#ed013e" : "#444444"}
            style={{
              width: prospe.W <= 560 ? "30px" : "33px",
              height: prospe.W <= 560 ? "30px" : "33px",
              marginLeft: "-7px",
            }}
            href="#"
            onMouseEnter={() => setEL("gitlab_RED")}
            onMouseLeave={() => setEL("gitlab")}
          />
        </Box>
      </Box>
    </Fragment>
  );
};

export default Sozial;
