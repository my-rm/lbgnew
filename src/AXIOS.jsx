import axios from "axios";

const AXIOS = axios.create({
  baseURL: "https://lightnerbox-default-rtdb.firebaseio.com/",
});

export default AXIOS;
